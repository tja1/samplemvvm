package com.nac.lesson6k1uistory.view.fragment;

import android.view.View;

import com.nac.lesson6k1uistory.R;
import com.nac.lesson6k1uistory.base.BaseFragment;
import com.nac.lesson6k1uistory.view.model.M001MainViewModel;

public class M001MainFragment extends BaseFragment<M001MainViewModel> {
    public static final String TAG = M001MainFragment.class.getName();

    @Override
    public void backToPreviousScreen() {
        mCallBack.closeApp();
    }

    @Override
    protected Class<M001MainViewModel> getClassViewModel() {
        return M001MainViewModel.class;
    }

    @Override
    protected void initViews() {
        //TODO init views
        initData();
    }

    private void initData() {
        // do nothing
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m001_main;
    }

    @Override
    public void onClick(View view) {
        //TODO Handle click
    }
}
