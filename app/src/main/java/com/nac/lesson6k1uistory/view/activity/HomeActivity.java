package com.nac.lesson6k1uistory.view.activity;

import com.nac.lesson6k1uistory.R;
import com.nac.lesson6k1uistory.base.BaseAct;
import com.nac.lesson6k1uistory.base.BaseFragment;
import com.nac.lesson6k1uistory.view.fragment.M000SplashFragment;
import com.nac.lesson6k1uistory.view.model.HomeViewModel;

public final class HomeActivity extends BaseAct<HomeViewModel> {
    private static final String TAG = HomeActivity.class.getName();
    private String backTag;

    @Override
    protected Class<HomeViewModel> getClassViewModel() {
        return HomeViewModel.class;
    }

    @Override
    protected void initViews() {
        showFrg(TAG, M000SplashFragment.TAG);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if (currentTAG == null) {
            super.onBackPressed();
            return;
        }
        BaseFragment<?> frg = mFrg.get(currentTAG);
        if (frg == null) {
            super.onBackPressed();
            return;
        }
        frg.backToPreviousScreen();
    }
}