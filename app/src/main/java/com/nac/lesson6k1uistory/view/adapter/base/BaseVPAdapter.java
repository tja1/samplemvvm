package com.nac.lesson6k1uistory.view.adapter.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.nac.lesson6k1uistory.R;

import java.util.List;

public abstract class BaseVPAdapter<T> extends PagerAdapter {
    protected Context mContext;
    protected List<T> listData;

    public BaseVPAdapter(Context mContext, List<T> listData) {
        this.mContext = mContext;
        this.listData = listData;
    }

    @Override
    public final int getCount() {
        return listData.size();
    }

    @NonNull
    @Override
    public final Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(getLayoutID(), container, false);
        T story = listData.get(position);
        initView(view, story);

        container.addView(view);
        return view;
    }

    protected abstract int getLayoutID();

    protected abstract void initView(View view, T data);

    @Override
    public final void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public final boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    protected final List<T> getListData() {
        return listData;
    }
}
