package com.nac.lesson6k1uistory.view.adapter;

import android.content.Context;
import android.view.View;

import com.nac.lesson6k1uistory.R;
import com.nac.lesson6k1uistory.view.adapter.base.BaseVPAdapter;

import java.util.List;

public class TextVPAdapter extends BaseVPAdapter<String> {
    public TextVPAdapter(Context mContext, List<String> listData) {
        super(mContext, listData);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.item_vp_text;
    }

    @Override
    protected void initView(View view, String content) {

    }
}
