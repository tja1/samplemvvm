package com.nac.lesson6k1uistory.view.adapter.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class BaseAdapter<T, R extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<R> {
    protected List<T> data;
    protected Context mContext;
    protected OnRCallBack<T> mCallBack;

    public BaseAdapter(List<T> data, Context mContext) {
        this.data = data;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public final R onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(getLayoutID(), parent, false);
        return getViewHolder(view);
    }

    protected abstract R getViewHolder(View view);

    protected abstract int getLayoutID();

    @Override
    public final void onBindViewHolder(@NonNull R holder, int position) {
        initData(holder, position);
    }

    protected abstract void initData(R holder, int position);

    @Override
    public final int getItemCount() {
        return data.size();
    }

    public final void setCallBack(OnRCallBack<T> callBack) {
        mCallBack = callBack;
    }

    public final List<T> getData() {
        return data;
    }

    public final void setData(List<T> data) {
        this.data = data;
    }

    public interface OnRCallBack<T> {
        void callBack(T data);
    }
}
