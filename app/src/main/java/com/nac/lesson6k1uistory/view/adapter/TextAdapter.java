package com.nac.lesson6k1uistory.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nac.lesson6k1uistory.R;
import com.nac.lesson6k1uistory.view.adapter.base.BaseAdapter;

import java.util.List;

public class TextAdapter extends BaseAdapter<String, TextAdapter.StringHolder> {

    public TextAdapter(List<String> data, Context mContext) {
        super(data, mContext);
    }

    @Override
    protected StringHolder getViewHolder(View view) {
        return new StringHolder(view);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.item_rv_text;
    }

    @Override
    protected void initData(StringHolder holder, int position) {
        String story = data.get(position);
        holder.tvContent.setText(story);
        holder.content = story;
    }

    public class StringHolder extends RecyclerView.ViewHolder {
        TextView tvContent;
        LinearLayout lnStory;
        String content;

        public StringHolder(@NonNull View itemView) {
            super(itemView);
            tvContent = itemView.findViewById(R.id.tv_content);
            lnStory = itemView.findViewById(R.id.ln_content);
            lnStory.setOnClickListener(view -> mCallBack.callBack(content));
        }
    }
}
