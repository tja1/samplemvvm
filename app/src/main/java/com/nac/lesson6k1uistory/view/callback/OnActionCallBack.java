package com.nac.lesson6k1uistory.view.callback;

public interface OnActionCallBack {
    void callBack(Object data, String key);
}
