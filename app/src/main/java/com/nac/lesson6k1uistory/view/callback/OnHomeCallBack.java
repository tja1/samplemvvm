package com.nac.lesson6k1uistory.view.callback;

public interface OnHomeCallBack {
    void showFrg(String backTag, String tag);
    void showFrg(String backTag, Object data, String tag);
    void closeApp();
}
