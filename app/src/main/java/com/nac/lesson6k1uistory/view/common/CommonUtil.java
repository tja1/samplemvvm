package com.nac.lesson6k1uistory.view.common;

public final class CommonUtil {
    private static CommonUtil instance;

    private CommonUtil() {
        //for singleton
    }

    public static CommonUtil getInstance() {
        if (instance == null) {
            instance = new CommonUtil();
        }
        return instance;
    }
}
