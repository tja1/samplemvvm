package com.nac.lesson6k1uistory.view.fragment;

import android.view.View;

import com.nac.lesson6k1uistory.App;
import com.nac.lesson6k1uistory.R;
import com.nac.lesson6k1uistory.base.BaseFragment;
import com.nac.lesson6k1uistory.view.model.M002ViewModel;

public class M002DetailFragment extends BaseFragment<M002ViewModel> {
    public static final String TAG = M002DetailFragment.class.getName();

    @Override
    public void defineBackKey() {
        if (tagSource.equals(M001MainFragment.TAG)) {
            App.getInstance().defineBackTag(TAG, M001MainFragment.TAG);
        }
    }

    @Override
    public void backToPreviousScreen() {
        String backTag = App.getInstance().doBackFlowWithBackTag(TAG);
        if (backTag.equals(M001MainFragment.TAG)) {
            mCallBack.showFrg(TAG, M001MainFragment.TAG);
        }
    }

    @Override
    protected Class<M002ViewModel> getClassViewModel() {
        return M002ViewModel.class;
    }

    @Override
    protected void initViews() {
        //TODO init views
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m002_detail;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back) {
            backToPreviousScreen();
        }
    }
}
