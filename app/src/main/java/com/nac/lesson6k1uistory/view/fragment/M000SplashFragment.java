package com.nac.lesson6k1uistory.view.fragment;

import android.os.Handler;

import com.nac.lesson6k1uistory.R;
import com.nac.lesson6k1uistory.base.BaseFragment;
import com.nac.lesson6k1uistory.view.model.M000SplashViewModel;

public class M000SplashFragment extends BaseFragment<M000SplashViewModel> {
    public static final String TAG = M000SplashFragment.class.getName();

    @Override
    public void backToPreviousScreen() {
        mCallBack.closeApp();
    }

    @Override
    protected Class<M000SplashViewModel> getClassViewModel() {
        return M000SplashViewModel.class;
    }

    @Override
    protected void initViews() {
        new Handler().postDelayed((Runnable) () -> {
            mCallBack.showFrg(TAG, M001MainFragment.TAG);
        }, 2000);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m000_splash;
    }
}
