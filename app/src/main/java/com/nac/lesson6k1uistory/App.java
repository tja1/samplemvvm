package com.nac.lesson6k1uistory;

import android.app.Application;
import android.util.Log;

import androidx.room.Room;

import com.nac.lesson6k1uistory.db.DBRoom;

import java.util.HashMap;
import java.util.Map;

public final class App extends Application {
    private static final String TAG = App.class.getName();
    private static final String UNDEFINE_TAG = "UNDEFINE_TAG";
    private static App instance;
    private Storage storage;
    private DBRoom db;
    private Map<String, String> mBackFlow;

    public static App getInstance() {
        return instance;
    }

    public DBRoom getDb() {
        return db;
    }

    public void defineBackTag(String key, String backTag) {
        mBackFlow.put(key, backTag);
    }

    public String doBackFlowWithBackTag(String key) {
        Log.i(TAG, "doBackFlowWithBackTag " + mBackFlow.toString());
        if (mBackFlow.containsKey(key)) {
            String backTag = mBackFlow.get(key);
            mBackFlow.remove(key);
            return backTag;
        }
        return UNDEFINE_TAG;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBackFlow = new HashMap<>();
        storage = new Storage();
        instance = this;
        db = Room.databaseBuilder(this, DBRoom.class, "Sample.db")
                /*.createFromAsset("databases/Sample.db")*/.build();
    }

    public Storage getStorage() {
        return storage;
    }
}
