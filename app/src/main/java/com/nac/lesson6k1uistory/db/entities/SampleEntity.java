package com.nac.lesson6k1uistory.db.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "Sample", primaryKeys = {"id"})
public class SampleEntity {
    @NonNull
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "content")
    private String content;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
