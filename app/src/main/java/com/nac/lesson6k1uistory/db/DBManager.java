package com.nac.lesson6k1uistory.db;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.nac.lesson6k1uistory.App;

public final class DBManager {
    private static final String TAG = DBManager.class.getName();
    private static DBManager instance;
    protected final Handler mHandler = new Handler(msg -> {
        if (msg.obj instanceof Object[]) {
            OnDBCallBack cb = (OnDBCallBack) ((Object[]) msg.obj)[0];
            cb.onCallBack(((Object[]) msg.obj)[1]);
        }
        return true;
    });

    private DBManager() {
        //for singleton
    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public void getAllData(final OnDBCallBack cb) {
        Log.i(TAG, "getAllTopic...");
        new Thread(() -> sendToHandler(cb, App.getInstance().getDb().getMediaDAO().getAllSample())).start();
    }

    private void sendToHandler(OnDBCallBack cb, Object data) {
        Message msg = new Message();
        msg.obj = new Object[]{cb, data};
        mHandler.sendMessage(msg);
    }

    public interface OnDBCallBack {
        void onCallBack(Object data);
    }
}
