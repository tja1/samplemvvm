package com.nac.lesson6k1uistory.db;

import androidx.room.Dao;
import androidx.room.Query;

import com.nac.lesson6k1uistory.db.entities.SampleEntity;

import java.util.List;

@Dao
public interface RoomDAO {
    @Query("SELECT * FROM Sample")
    List<SampleEntity> getAllSample();
}
