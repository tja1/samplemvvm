package com.nac.lesson6k1uistory.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.nac.lesson6k1uistory.db.entities.SampleEntity;

@Database(version = 1, entities = {SampleEntity.class})
public abstract class DBRoom extends RoomDatabase {
    public abstract RoomDAO getMediaDAO();
}
