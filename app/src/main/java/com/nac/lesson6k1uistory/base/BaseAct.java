package com.nac.lesson6k1uistory.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.nac.lesson6k1uistory.App;
import com.nac.lesson6k1uistory.R;
import com.nac.lesson6k1uistory.Storage;
import com.nac.lesson6k1uistory.view.callback.OnHomeCallBack;

import java.lang.reflect.Constructor;
import java.util.HashMap;

public abstract class BaseAct<T extends ViewModel> extends AppCompatActivity
        implements View.OnClickListener, OnHomeCallBack {
    protected final HashMap<String, BaseFragment<?>> mFrg = new HashMap<>();
    protected T mModel;
    protected String currentTAG;

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        mModel = new ViewModelProvider(this).get(getClassViewModel());

        initViews();
    }

    protected abstract Class<T> getClassViewModel();

    @Override
    public final <G extends View> G findViewById(int id) {
        return findViewById(id, null);
    }

    public final <K extends View> K findViewById(int id, View.OnClickListener event) {
        K view = super.findViewById(id);

        if (view != null && event != null) {
            view.setOnClickListener(event);
        }
        return view;
    }

    protected abstract void initViews();

    protected abstract int getLayoutId();

    @Override
    public void onClick(View view) {
        //do nothing
    }

    protected final Storage getStorage() {
        return App.getInstance().getStorage();
    }

    public void setSavedFrg(String key, BaseFragment<?> preTag) {
        mFrg.put(key, preTag);
    }

    @Override
    public void showFrg(String backTag, String tag) {
        showFrg(backTag, null, tag);
    }

    @Override
    public void showFrg(String tagSource, Object data, String tagChild) {
        try {
            Class<?> clazz = Class.forName(tagChild);
            Constructor<?> constructor = clazz.getConstructor();
            BaseFragment<?> frg = (BaseFragment<?>) constructor.newInstance();
            frg.setCallBack(this);
            frg.setData(data);
            frg.setTagSource(tagSource);

            currentTAG = tagChild;
            setSavedFrg(tagChild, frg);

            getSupportFragmentManager()
                    .beginTransaction().replace(R.id.ln_main, frg, tagChild)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closeApp() {
        finish();
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }
}
